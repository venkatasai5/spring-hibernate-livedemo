<!DOCTYPE HTML>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.opt.pojos.Users"%>
<html>
<body>
	<script>
		function changeTest() {
			var Index = document.menuForm.select1.options[document.menuForm.select1.selectedIndex].value;
			if (Index == 0) {
				document.getElementById("menuForm").action = "Admin";
				document.getElementById("menuForm").submit();
			} else if (Index == 1) {
				document.getElementById("menuForm").action = "ViewUsers/1";
				document.getElementById("menuForm").submit();
			} else if (Index == 2) {
				document.getElementById("menuForm").action = "AddProducts";
				document.getElementById("menuForm").submit();
			} else if (Index == 3) {
				document.getElementById("menuForm").action = "ViewProducts";
				document.getElementById("menuForm").submit();
			} else if (Index == 4) {
				document.getElementById("menuForm").action = "j_spring_security_logout";
				document.getElementById("menuForm").submit();
			}
		}
		function UpdateProfile(Userid) {
			document.getElementById("menuForm1").action = "UpdateProfile";
			document.getElementById("menuForm1").submit();
		}
	</script>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Online Product Tour</title>
</head>
<form method="POST" id="menuForm" name="menuForm">
	<div style="text-align: center">
		<div style="text-align: center">
			<h2>
				<font color=" #f99600">Online Product Tour</font>
			</h2>
		</div>
		<p>
			<strong>Select your favorite species!</strong>
		</p>

		<select id="select1" onchange="changeTest()" name="select1">

			<option value="0" selected="selected">Home</option>
			<option value="1">View Users</option>
			<option value="2">AddProducts</option>
			<option value="3">ViewProducts</option>
			<option value="4">Logout</option>
		</select>
	</div>
</form>
<%
	Users lp = (Users) session.getAttribute("Amsg");
%>
<div style="text-align: left; margin: 5em">
	<h2>
		Welcome
		<%=lp.getUname()%>
	</h2>
	<p>Administrator, a person responsible for running technically
		advanced information systems!!</p>
</div>
<div style="text-align: center">
	<h4>Profile</h4>
	<form method="POST" id="menuForm1" name="menuForm1">
		<table width="750px" align="center" border="3">
			<tr>
				<th>Admin Name</th>
				<th>Password</th>
				<th>Email</th>
				<th>Role</th>
			</tr>
			<tr>
				<td><%=lp.getUname()%></td>
				<td><%=lp.getPwd()%></td>
				<td><%=lp.getEmail()%></td>
				<td><%=lp.getProle()%></td>
			</tr>
		</table>
		<br> <input type="submit" id="<%=lp.getEmail()%>" value="Update"
			onclick="UpdateProfile(this.id)">
	</form>
	${Amsg1}
</div>
</body>
</html>