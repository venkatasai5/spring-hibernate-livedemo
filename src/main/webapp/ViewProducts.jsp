<!DOCTYPE HTML>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
	<script>
		function changeTest() {
			var Index = document.menuForm.select1.options[document.menuForm.select1.selectedIndex].value;
			if (Index == 0) {
				document.getElementById("menuForm").action = "Admin";
				document.getElementById("menuForm").submit();
			} else if (Index == 1) {
				document.getElementById("menuForm").action = "ViewUsers/1";
				document.getElementById("menuForm").submit();
			} else if (Index == 2) {
				document.getElementById("menuForm").action = "AddProducts";
				document.getElementById("menuForm").submit();
			} else if (Index == 3) {
				document.getElementById("menuForm").action = "ViewProducts";
				document.getElementById("menuForm").submit();
			} else if (Index == 4) {
				document.getElementById("menuForm").action = "j_spring_security_logout";
				document.getElementById("menuForm").submit();
			}
		}
		function searchUsers() {

			document.getElementById("menuForm1").action = "searchItems";
			document.getElementById("menuForm1").submit();
		}
		function deleteUsers() {

			document.getElementById("menuForm1").action = "deleteItems";
			document.getElementById("menuForm1").submit();
		}
	</script>
<head>
<meta charset="UTF-8">
<title>Online Product Tour</title>
</head>
<form method="post" id="menuForm" name="menuForm">
	<div style="text-align: center">
		<div style="text-align: center">
			<h2>
				<font color=" #f99600">Online Product Tour</font>
			</h2>
		</div>
		<p>
			<strong>Select your favorite species!</strong>
		</p>

		<select id="select1" onchange="changeTest()" name="select1">

			<option value="0">Home</option>
			<option value="1">View Users</option>
			<option value="2">AddProducts</option>
			<option value="3" selected="selected">ViewProducts</option>
			<option value="4">Logout</option>
		</select>
	</div>
</form>
<div align="center">
	<h2></h2>
	<form action="" id="menuForm1" name="menuForm1" method="post">
		<table>
			<tr>
				<td><select name="company">
						<option>Select Company</option>
						<c:forEach var="item" items="${msg}">
							<option value="${item.company}">${item.company}</option>
						</c:forEach>
				</select> ${SearchItems}</td>
				<td><select name="category">
						<option>Select Category</option>
						<c:forEach var="item" items="${msg}">
							<option value="${tem.category}">${item.category}</option>
						</c:forEach>
				</select> ${SearchItems}</td>
				<td><input type="submit" value="Search" onclick="searchUsers()">
				</td>
			</tr>
		</table>
		<div
			style="text-align: center; height: 300px; width: 950px; border: solid black;">
			<table border="3" width="950px" align="center">
				<tr>
					<th><font color="black">Select</font></th>
					<th><font color="black">Company Name</font></th>
					<th><font color="black">Category Name</font></th>
					<th><font color="black">Item Name</font></th>
					<th><font color="black">Quantity</font></th>
					<th><font color="black">Price</font></th>
					<th><font color="black">Features</font></th>
					<th><font color="black">Links</font></th>
				</tr>
				<c:forEach items="${itmes}" var="items">
					<tbody>
						<tr>
							<td><input type="checkbox" id="${items.itemId}"
								name="checkId" value="${items.itemId}" /></td>
							<td><c:out value="${items.companyid.COMPANYNAME}" /></td>
							<td><c:out value="${items.categeryid.CATEGORYNAME}" /></td>
							<td><c:out value="${items.item}" /></td>
							<td><c:out value="${items.quantity}" /></td>
							<td><c:out value="${items.price}" /></td>
							<td><c:out value="${items.featuers}" /></td>
							<td><c:out value="${items.links}" /></td>
						</tr>
					</tbody>
				</c:forEach>
			</table>
		</div>
		<br> <input type="submit" value="Delete" onclick="deleteUsers()"><font
			color="#FF0000"> ${DeleteItems}</font>
		<div
			style="text-align: center; width: align-self; height: 15px">
			<a href="/SpringMVC/viewemp/1">1</a> <a href="/SpringMVC/viewemp/2">2</a>
			<a href="/SpringMVC/viewemp/3">3</a>
		</div>
	</form>
</div>
</body>
</html>