<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta>
<title>Login-Online Product Tour</title>
<script type="text/javascript">
	function loginvalidate() {
		document.forms[1].action = "<c:url value='j_spring_security_check' />";
		document.forms[1].submit();
	}
	function registervalidate() {
		document.forms[2].action = "RegisterValidate"
		document.forms[2].submit();
	}
	function Home() {
		document.forms[0].action = "${pageContext.request.contextPath}/";
		document.forms[0].submit();
	}
</script>
</head>
<body onload='document.forms[1].email.focus();'>
	<form action="" method="POST">
		<center>
			<h1>
				<font color=" #f99600" onclick="Home()">Online Product Tour</font>
			</h1>
		</center>
	</form>
	<br>
	<br>
	<br>
	<br>
	<div style="float: left; margin: 5em">
		<p>Please Enter Your Valid Details In Order To Login To Your Home
			Page.</p>
		<form method="post" onSubmit="validate_form();">
			<table>
				<tr>
					<td>Email Or PhoneNo:</td>
					<td><input type="text" name="email" value="${uname}"></td>
				</tr>
				<tr>
					<td>Password :</td>
					<td><input type="password" name="pwd"></td>
				</tr>
				<tr>
					<td><input type="reset" value="Reset"></td>
					<td><input type="submit" value="Login"
						onclick="loginvalidate()"></td>
				</tr>
			</table>
			<br>
			<div
				style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #a94442; background-color: #f2dede; border-color: #ebccd1;">
				<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
					<font color="red"> UsernameOrPassword Incorrect!!<br />
					</font>
				</c:if>
				<font color="#FF0000">${deneid}${Loginmsg}${pwd}</font>
			</div>
		</form>
	</div>
	<div style="float: right; margin: 5em">
		<p>Please Enter The Following User Information to Register!!!</p>
		<form action="" method="post" onSubmit="return validate_form(this);">
			<table>
				<tr>
					<td>User Name:</td>
					<td><input type="text" name="uname" value="${name}"></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type="password" name="pwd"></td>
				</tr>
				<tr>
					<td>Date Of Birth:</td>
					<td><input type="text" name="dob" value="${dob}"></td>
				</tr>
				<tr>
					<td>Email ID:</td>
					<td><input type="text" name="email" value="${email}"></td>
				</tr>
				<tr>
					<td>Phone No.:</td>
					<td><input type="text" name="phno" value="${phno}"></td>
				</tr>
				<tr>
					<td><input type="reset" value="Reset"></td>
					<td><input type="submit" value="Register"
						onclick="registervalidate()"></td>
				</tr>
			</table>
			<br>
			<%
				session.invalidate();
			%>
			<div
				style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #a94442; background-color: #f2dede; border-color: #ebccd1;">
				<font color="#FF0000">${emailmsg2}</font> <font color="#FF0000">${emailmsg1}</font>
				<font color="#FF0000">${Regmsg}</font>
			</div>
		</form>
	</div>
</body>
</html>