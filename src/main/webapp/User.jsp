<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<body>
	<script>
		function changeTest() {
			var Index = document.menuForm1.select1.options[document.menuForm1.select1.selectedIndex].value;
			if (Index == 0) {
				document.getElementById("menuForm1").action = "Home";
				document.getElementById("menuForm1").submit();
			} else if (Index == 1) {
				document.getElementById("menuForm1").action = "Profile";
				document.getElementById("menuForm1").submit();
			} else if (Index == 2) {
				document.getElementById("menuForm1").action = "j_spring_security_logout";
				document.getElementById("menuForm1").submit();
			}
		}
		function Search() {
			document.getElementById("menuForm1").action = "Search";
			document.getElementById("menuForm1").submit();
		}
		function Home() {
			document.getElementById("menuForm1").action = "Home";
			document.getElementById("menuForm1").submit();
		}
	</script>
<head>
<meta charset="UTF-8">
<title>Online Product Tour</title>
</head>

<form method="POST" id="menuForm1" name="menuForm1">
	<div style="text-align: left; padding-bottom: 0cm; margin: 1em">
		<h1>
			<font color="#f99600" onclick="Home()">Online Product Tour</font>
		</h1>
	</div>
	<div
		style="width: 20%; position: absolute; left: 50%; transform: translate(-50%, -50%); text-align: center;">
		<div>
			<input type="text"
				style="float: left; width: 90%; border: 1px solid #f99600; padding: 5px; border-radius: 5px;"
				placeholder="What are you looking for?">
			<button type="submit" class="searchButton"
				style="width: 5%; border: 1px solid; position: absolute; width: 50px; height: 25px; border: 1px solid #f99600; text-align: center; border-radius: 5px; cursor: pointer; font-size: 12px;"
				onclick="Search()">Search</button>
		</div>
	</div>
	<div style="text-align: right; margin: 1em">
		<select id="select1" onchange="changeTest()" name="select1">
			<option value="0" selected="selected">Home</option>
			<option value="1">Profile</option>
			<option value="2">Logout</option>
		</select>
	</div>
</form>


<aside style="font-weight: bold; float: left; margin: 1em">
	<form method="POST" id="menuForm2" name="menuForm2">
		<div
			style="overflow: auto; text-align: center; height: 210px; width: 200px; border: 1px solid #f99600; padding: 5px; border-radius: 5px">
			<table>
				<thead>
					<tr>
						<th colspan="2"><h4
								style="border-bottom: 1px solid; margin: 0.5em">
								<font color=" #f99600">Company</font>
							</h4></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${Companies}" var="items">
						<tr>
							<td><input type="checkbox" id="${items.COMPANYID}"
								name="checkId" value="${items.COMPANYID}" /></td>
							<td align="left"><a href="">${items.COMPANYNAME}</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<br>
		<div
			style="overflow: auto; text-align: center; height: 210px; width: 200px; border: 1px solid #f99600; padding: 5px; border-radius: 5px">
			<table>
				<thead>
					<tr>
						<th colspan="2"><h4
								style="border-bottom: 1px solid; margin: 0.5em">
								<font color=" #f99600">Category</font>
							</h4></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${Categeries}" var="items">
						<tr>
							<td><input type="checkbox" id="${items.CATEGORYID}"
								name="checkId" value="${items.CATEGORYID}" /></td>
							<td align="left"><a href="">${items.CATEGORYNAME}</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</form>
</aside>


<form method="POST" id="menuForm3" name="menuForm3">
	<div
		style="font-weight: bold; margin-top: 2em; overflow: auto; float: center; width: 700px; height: 440px; border: 1px solid #f99600; border-radius: 5px">
		<table style="padding: 5px; width: 600px; height: 440px;">
			<c:forEach items="${Items}" var="items">
				<tr>
					<td align="left"><span><strong><c:out
									value="${items.item}" /></strong></span></td>
					<td align="right"><span><a href="">Show Details</a></span></td>
				</tr>
				<tr>
					<td colspan="2" align="left"><span><strong>Features:</strong></span>
						<c:out value="${items.featuers}" /></td>
				</tr>
				<tr style="border-bottom: 2px solid black">
					<td align="left"><span><strong>Price(INR):</strong></span> <c:out
							value="${items.price}" /></td>
					<td align="center"><span><strong>ItemCode:</strong></span> <c:out
							value="${items.itemId}" /></td>
				</tr>
			</c:forEach>
		</table>
		<br>
	</div>
	<div
		style="text-align: center; margin: 2em; margin-left: 18em; width: align-self; height: 10px">
		<a href="/SpringMVC/viewemp/1"></a>
	</div>
</form>
</body>
</html>