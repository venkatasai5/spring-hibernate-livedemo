<!DOCTYPE HTML>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="UTF-8">
<title>Online Product Tour</title>
</head>
<script>
	function changeTest() {
		var Index = document.menuForm.select1.options[document.menuForm.select1.selectedIndex].value;
		if (Index == 0) {
			document.getElementById("menuForm").action = "${pageContext.request.contextPath}/Admin";
			document.getElementById("menuForm").submit();
		} else if (Index == 1) {
			document.getElementById("menuForm").action = "ViewUsers/1";
			document.getElementById("menuForm").submit();
		} else if (Index == 2) {
			document.getElementById("menuForm").action = "${pageContext.request.contextPath}/AddProducts";
			document.getElementById("menuForm").submit();
		} else if (Index == 3) {
			document.getElementById("menuForm").action = "${pageContext.request.contextPath}/ViewProducts";
			document.getElementById("menuForm").submit();
		} else if (Index == 4) {
			document.getElementById("menuForm").action = "j_spring_security_logout";
			document.getElementById("menuForm").submit();
		}
	}
	function searchUsers() {

		document.getElementById("menuForm1").action = "searchUsers";
		document.getElementById("menuForm1").submit();
	}
	function deleteUsers() {

		document.getElementById("menuForm1").action = "deleteUsers";
		document.getElementById("menuForm1").submit();
	}
</script>
<body>

	<form method="POST" id="menuForm" name="menuForm">
		<div style="text-align: center">
			<div style="text-align: center">
				<h2>
					<font color=" #f99600">Online Product Tour</font>
				</h2>
			</div>
			<p>
				<strong>Select your favorite species!</strong>
			</p>

			<select id="select1" onchange="changeTest()" name="select1">

				<option value="0">Home</option>
				<option value="1" selected="selected">View Users</option>
				<option value="2">AddProducts</option>
				<option value="3">ViewProducts</option>
				<option value="4">Logout</option>
			</select>
		</div>
	</form>
	<div align="center">
		<h2>View Registered Users</h2>
		<form action="" id="menuForm1" name="menuForm1" method="POST">
			<div>
				<input type="text" name="search" placeholder="Search for Users..">
				<input type="submit" value="Search" onclick="searchUsers()">
				<br> ${SearchUsers} <br>
			</div>
			<div
				style="text-align: center; height: 300px; width: 900px; border: solid black;">
				<table width="900px" align="center" border="3">

					<tr>
						<th><font color="black">Select</font></th>
						<th><font color="black">User Name</font></th>
						<th><font color="black">Password</font></th>
						<th><font color="black">Date Of Birth</font></th>
						<th><font color="black">Email Id</font></th>
						<th><font color="black">PhoneNumber</font></th>
					</tr>
					<c:forEach items="${ViewUsers}" var="users">
						<tbody>
							<tr>
								<td><input type="checkbox" id="${users.email}"
									name="checkId" value="${users.email}" /></td>
								<td><c:out value="${users.uname}" /></td>
								<td><c:out value="${users.pwd}" /></td>
								<td><c:out value="${users.dob}" /></td>
								<td><c:out value="${users.email}" /></td>
								<td><c:out value="${users.phno}" /></td>
							</tr>
						</tbody>
					</c:forEach>
				</table>
			</div>
			<br> <input type="submit" value="Delete" onclick="deleteUsers()">
			<font color="#FF0000">${DeleteUsers}</font>
			<div style="text-align: center; width: align-self; height: 10px">
				<a href="/opt/ViewUsers/1">1</a> <a href="/opt/ViewUsers/2">2</a> <a
					href="/opt/ViewUsers/3">3</a>
			</div>
		</form>
	</div>
</body>
</html>