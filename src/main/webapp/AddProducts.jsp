<%@page import="org.hibernate.cfg.AnnotationConfiguration"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script type="text/javascript">
	function validate_form(formObj) {
		if (formObj.company.value == 0) {
			alert("Please Select Company");
			formObj.com.focus();
			return false;
		}
		/* if (formObj.category.value.length == 0) {
			alert("Please Select Category");
			formObj.cat.focus();
			return false;
		} */
		if (formObj.item.value == 0) {
			alert("Please Enter Model No");
			formObj.item.focus();
			return false;
		}

		if (formObj.quantity.value == 0) {
			alert("Please Enter quantity");
			formObj.quantity.focus();
			return false;
		}
		if (formObj.price.value == 0) {
			alert("Please Enter Price");
			formObj.price.focus();
			return false;
		}

		if (formObj.features.value == 0) {
			alert("Please Enter Atleast One feature");
			formObj.features.focus();
			return false;
		}

		return true;
	}
	function changeTest() {
		var Index = document.menuForm.select1.options[document.menuForm.select1.selectedIndex].value;
		if (Index == 0) {
			document.getElementById("menuForm").action = "Admin";
			document.getElementById("menuForm").submit();
		} else if (Index == 1) {
			document.getElementById("menuForm").action = "ViewUsers/1";
			document.getElementById("menuForm").submit();
		} else if (Index == 2) {
			document.getElementById("menuForm").action = "AddProducts";
			document.getElementById("menuForm").submit();
		} else if (Index == 3) {
			document.getElementById("menuForm").action = "ViewProducts";
			document.getElementById("menuForm").submit();
		} else if (Index == 4) {
			document.getElementById("menuForm").action = "j_spring_security_logout";
			document.getElementById("menuForm").submit();
		}
	}
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta content="text/html; charset=UTF-8">
<title>Online Product Tour</title>
</head>
<body>
	<form method="post" id="menuForm" name="menuForm">
		<div style="text-align: center">
			<div style="text-align: center">
				<h2>
					<font color=" #f99600">Online Product Tour</font>
				</h2>
			</div>
			<p>
				<strong>Select your favorite species!</strong>
			</p>

			<select id="select1" onchange="changeTest()" name="select1">

				<option value="0">Home</option>
				<option value="1">View Users</option>
				<option value="2" selected="selected">AddProducts</option>
				<option value="3">ViewProducts</option>
				<option value="4">Logout</option>
			</select>
		</div>
	</form>

	<div style="float: left; margin: 5em">
		<form action="AddCompanyAndCategory" method="post"
			onSubmit="return validate_form(this);">
			<table cellspacing="1" cellpadding="2" align="left" border="1">
				<tr>
					<th><font color="black">Enter Company Name </font></th>
					<td><input type="text" name="COMPANYNAME"></td>
				</tr>
				<tr>
					<th><font color="black">Enter Category Name</font></th>
					<td><input type="text" name="CATEGORYNAME"></td>
				</tr>
				<tr>
					<td><input type="reset" value="Reset"></td>
					<td><input type="submit" value="Submit"></td>
				</tr>
			</table>
			<br> <font color="#FF0000">${AddCCError}</font>
		</form>
	</div>
	<div style="float: right; margin: 5em">
		<form method="post" action="itemadd"
			onSubmit="return validate_form(this);">
			<table cellspacing="1" align="Right" border="1">
				<tr>
					<th><font color="black">Company</font></th>
					<td><select name="COMPANYID">
							<option>Select Company</option>
							<c:forEach var="company" items="${CompanyList}">
								<option value="${company.COMPANYID}">${company.COMPANYNAME}</option>
							</c:forEach>
					</select></td>
				</tr>

				<tr>
					<th><font color="black">Category</font></th>
					<td><select name="CATEGORYID">
							<option>Select Category</option>
							<c:forEach var="category" items="${CategoryList}">
								<option value="${category.CATEGORYID}">${category.CATEGORYNAME}</option>
							</c:forEach>

					</select></td>
				</tr>

				<tr>
					<th><font color="black">Item name:</font></th>
					<td><input type="text" name="item"></td>
				</tr>
				<tr>
					<th><font color="black">Quantity:</font></th>
					<td><input type="text" name="quantity"></td>
				</tr>
				<tr>
					<th><font color="black">Price:</font></th>
					<td><input type="text" name="price"></td>
				</tr>
				<tr>
					<th><font color="black">Features:</font></th>
					<td><input type="text" name="featuers"></td>
				</tr>
				<tr>
					<th><font color="black">Links:</font></th>
					<td><textarea name="links" rows="5" cols="18"></textarea></td>
				</tr>
				<tr>
					<td><input type="reset" value="Reset"></td>
					<td><input type="submit" value="Submit"></td>
				</tr>
			</table>
			${msg}
		</form>
	</div>
</body>
</html>