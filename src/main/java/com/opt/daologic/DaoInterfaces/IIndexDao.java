package com.opt.daologic.DaoInterfaces;

import java.util.List;

import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;
import com.opt.pojos.Users;

public interface IIndexDao {

	public Users RegisterSave(Users users);

	public Users LoginValidate(String uname);

	public List<Companies> CompanyList();

	public List<Categeries> CategeriesList();

	public List<Items> ItemList();

	public void Logout();
}
