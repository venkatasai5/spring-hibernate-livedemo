package com.opt.daologic.DaoInterfaces;

import java.util.List;

import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;

public interface IAddProductsDao {

	public String addCampany(Companies companies);

	public String addCategory(Categeries catgeries);

	public List<Companies> ViewCompanies();

	public List<Categeries> ViewCategories();

	public String addItem(Companies company, Categeries category, Items items);
}
