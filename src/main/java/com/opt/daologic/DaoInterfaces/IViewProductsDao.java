package com.opt.daologic.DaoInterfaces;

import java.util.List;

import com.opt.pojos.Items;

public interface IViewProductsDao {

	public List<Items> ViewProducts();
}
