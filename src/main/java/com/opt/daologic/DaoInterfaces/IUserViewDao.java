package com.opt.daologic.DaoInterfaces;

import java.util.List;

import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;

public interface IUserViewDao {
	public List<Items> ItemList();

	public List<Categeries> CategeriesList();

	public List<Companies> CompanyList();
}
