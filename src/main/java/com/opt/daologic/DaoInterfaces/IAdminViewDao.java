package com.opt.daologic.DaoInterfaces;

import com.opt.pojos.Users;

public interface IAdminViewDao {

	public Users ProfileUpdateAction(String uname);

	public Users ProfileUpdateProcess(Users users);
}
