package com.opt.daologic.DaoInterfaces;

import java.util.List;

import com.opt.pojos.Users;

public interface IViewUsersDao {

	public List<Users> ViewUsers(int pageid,int total);

	public List<Users> DeleteUsers(String[] Id);

	public List<Users> SearchUsers(String uname);
}
