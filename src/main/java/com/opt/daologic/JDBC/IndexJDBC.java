package com.opt.daologic.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;

import com.opt.daologic.Hibernate.IndexDao;

public class IndexJDBC {
	private final static Logger logger = Logger.getLogger(IndexDao.class);

	private static Connection Connection() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "SYSTEM",
					"root");
			logger.info("Connection Created");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
