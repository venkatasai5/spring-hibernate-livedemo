package com.opt.daologic.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;

import com.opt.daologic.Hibernate.AddProductsDao;

public class AddProductsJDBC {

	private final static Logger logger = Logger.getLogger(AddProductsDao.class);

	private static Connection Connection() {
		Connection connection;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "SYSTEM", "root");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
