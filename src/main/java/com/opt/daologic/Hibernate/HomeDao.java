package com.opt.daologic.Hibernate;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.classic.Session;

import com.opt.daologic.DaoInterfaces.IAdminViewDao;
import com.opt.pojos.Users;

public class HomeDao implements IAdminViewDao {
	private final static Logger logger = Logger.getLogger(HomeDao.class);

	private void init() {
		logger.info(HomeDao.class + " Initiated");
	}

	private static Session Connection() {
		SessionFactory sessionfact = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionfact.openSession();
		logger.info("Connection Created");
		return session;
	}

	public Users ProfileUpdateAction(String uname) {
		logger.info("Entered to profile Hibernate Update Action Method");
		Session connection = Connection();
		try {
			Users result = (Users) connection.get(Users.class, uname);
			return result;
		} catch (RuntimeException e) {
			throw new Exception("500", "Server Not Responding");
		} finally {
			connection.close();
		}
	}

	public Users ProfileUpdateProcess(Users users) {
		Session connection = Connection();
		connection.beginTransaction();
		connection.update(users);
		Users result = (Users) connection.get(Users.class, users.getEmail());
		connection.getTransaction().commit();
		return result;
	}

}
