package com.opt.daologic.Hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Component;

import com.opt.controllers.Index;
import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;

@Component
public class UserView {
	private final static Logger logger = Logger.getLogger(Index.class);

	private void init() {
		logger.info(UserView.class + " Initiated");
	}

	private static Session Connection() {
		SessionFactory sessionfact = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionfact.openSession();
		logger.info("Connection Created");
		return session;
	}

	public List<Companies> CompanyListHibernate() {
		logger.info("Entered Into hibernate companies List method");
		Session connection = Connection();
		connection.beginTransaction();
		List<Companies> Companies = connection.createQuery("from Companies").list();
		connection.getTransaction().commit();
		return Companies;
	}

	public List<Categeries> CategeriesListHibernate() {
		logger.info("Entered Into hibernate Category List method");
		Session connection = Connection();
		connection.beginTransaction();
		List<Categeries> categeries = connection.createQuery("from Categeries").list();
		connection.getTransaction().commit();
		return categeries;
	}

	public List<Items> ItemListHibernate() {
		logger.info("Entered Into hibernate Item List method");
		Session connection = Connection();
		connection.beginTransaction();
		List<Items> items = connection.createQuery("from Items").list();
		connection.getTransaction().commit();
		return items;
	}

}
