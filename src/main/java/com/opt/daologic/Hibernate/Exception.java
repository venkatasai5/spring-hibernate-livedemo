package com.opt.daologic.Hibernate;

public class Exception extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String errorcode;
	private String errormsg;

	public final String getErrorcode() {
		return errorcode;
	}

	public final void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public final String getErrormsg() {
		return errormsg;
	}

	public final void setErrormsg(String errormsg) {
		this.errormsg = errormsg;
	}

	public Exception(String errcode, String errmsg) {
		this.errorcode = errcode;
		this.errormsg = errmsg;
	}

}
