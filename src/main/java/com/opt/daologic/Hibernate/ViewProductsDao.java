package com.opt.daologic.Hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.classic.Session;

import com.opt.daologic.DaoInterfaces.IViewProductsDao;
import com.opt.pojos.Items;

public class ViewProductsDao implements IViewProductsDao {

	private final static Logger logger = Logger.getLogger(IndexDao.class);

	private void init() {
		logger.info(ViewProductsDao.class + " Initiated");
	}

	private static Session Connection() {
		SessionFactory sessionfact = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionfact.openSession();
		logger.info("Connection Created");
		return session;
	}

	public List<Items> ViewProducts() {
		Session connection = Connection();
		connection.beginTransaction();
		Query result = connection.createQuery("from Items");
		List<Items> Items = result.list();
		return Items;
	}
}
