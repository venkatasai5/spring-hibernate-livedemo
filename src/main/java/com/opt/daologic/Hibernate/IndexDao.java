package com.opt.daologic.Hibernate;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;

import com.opt.daologic.DaoInterfaces.IIndexDao;
import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;
import com.opt.pojos.Users;

public class IndexDao implements IIndexDao {
	private final static Logger logger = Logger.getLogger(IndexDao.class);

	private void init() {
		logger.info(IndexDao.class + " Initiated");
	}

	//@Autowired
	//private SessionFactory Session;

	private static Session Connection() {
		SessionFactory sessionfact = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionfact.openSession();
		logger.info("Connection Created");
		return session;
	}

	public Users RegisterSave(Users users) {
		logger.info("Entered to Hibernate Register Save Method");
		SimpleDateFormat sf = new SimpleDateFormat();
		Date date = new Date();
		Session connection = Connection();
		//Session connection = Session.openSession();
		try {
			users.setProle("User");
			users.setDate_time(sf.format(date));
			users.setCount(0);
			users.setStatus(1);
			Users result1 = (Users) connection.get(Users.class, users.getEmail().toString());
			if (result1 == null) {
				connection.beginTransaction();
				connection.save(users);
				connection.getTransaction().commit();
				Users result = (Users) connection.get(Users.class, users.getEmail());
				return result;
			} else {
				return result1;
			}
		} catch (Exception e) {
			throw new Exception("500", "Server Not Responding");
		} finally

		{
			connection.close();
		}
	}

	public Users LoginValidate(String uname) {
		logger.info("Entered Into hibernate Login Validate method");
		Session connection = Connection();
		//org.hibernate.classic.Session connection = Session.openSession();
		try {
			Query Result1 = connection.createQuery("from Users where email=:uname");
			Result1.setParameter("uname", uname);
			List<Users> profile = Result1.list();
			for (Users Profile : profile) {
				return Profile;
			}
			return null;
		} catch (Exception e) {
			logger.error("Exit Hibernate LoginValidate Catch Block");
			throw new Exception("500", "Server Not Responding");
		} finally {
			connection.close();
		}
	}

	public List<Companies> CompanyList() {
		logger.info("Entered Into hibernate companies List method");
		Session connection = Connection();
		//org.hibernate.classic.Session connection = Session.openSession();
		List<Companies> Companies = connection.createQuery("from Companies").list();
		return Companies;
	}

	public List<Categeries> CategeriesList() {
		logger.info("Entered Into hibernate Category List method");
		Session connection = Connection();
		//org.hibernate.classic.Session connection = Session.openSession();
		List<Categeries> categeries = connection.createQuery("from Categeries").list();
		return categeries;
	}

	public List<Items> ItemList() {
		logger.info("Entered Into hibernate Item List method");
		Session connection = Connection();
		//org.hibernate.classic.Session connection = Session.openSession();
		List<Items> items = connection.createQuery("from Items").list();
		return items;
	}

	public void Logout() {
		Session connection = Connection();
		//org.hibernate.classic.Session Connection = Session.openSession();
		connection.clear();
		connection.close();
	}
	public static void main(String[] args) throws HibernateException, SQLException {
		System.out.println(Connection().get(Users.class, "Dilip@gmail.com"));
	}
}
