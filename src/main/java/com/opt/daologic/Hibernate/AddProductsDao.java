package com.opt.daologic.Hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opt.daologic.DaoInterfaces.IAddProductsDao;
import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;

@Component
public class AddProductsDao implements IAddProductsDao {
	private final static Logger logger = Logger.getLogger(AddProductsDao.class);

	private void init() {
		logger.info(AddProductsDao.class + " Initiated");
	}

	// @Autowired
	// private SessionFactory Session;

	private static Session Connection() {
		SessionFactory sessionfact = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionfact.openSession();
		logger.info("Connection Created");
		return session;
	}

	public String addCampany(Companies companies) {
		logger.info("Entered Into HIbernate Add company Method" + companies.getCOMPANYNAME());
		if (companies.getCOMPANYNAME().isEmpty()) {
			return null;
		} else {
			Session session = Connection();
			//org.hibernate.classic.Session session = Session.openSession();
			Query result1 = session.createQuery("from Companies where COMPANYNAME=?");
			result1.setParameter(0, companies.getCOMPANYNAME());
			List<Companies> Result2 = result1.list();
			if (Result2.isEmpty()) {
				try {
					session.save(companies);
					session.beginTransaction().commit();
					return companies.getCOMPANYNAME() + " Added Successfully";
				} catch (HibernateException e) {
					logger.error("Entered Into AddCompanyHibernate CatchBlock");
					return " " + e + " ";
				}
			} else {
				return companies.getCOMPANYNAME() + " Already Exist";
			}
		}
	}

	public String addCategory(Categeries catgeries) {
		logger.info("Entered Into Hibernate Add category Method" + catgeries.getCATEGORYNAME());
		if (catgeries.getCATEGORYNAME().isEmpty()) {
			return null;
		} else {
			Session session = Connection();
			//org.hibernate.classic.Session session = Session.openSession();
			Query result1 = session.createQuery("from Categeries where CATEGORYNAME=?");
			result1.setParameter(0, catgeries.getCATEGORYNAME());
			List<Categeries> Result2 = result1.list();
			if (Result2.isEmpty()) {
				try {
					session.save(catgeries);
					session.beginTransaction().commit();
					return catgeries.getCATEGORYNAME() + " Added Successfully";
				} catch (HibernateException e) {
					logger.error("AddCategoryHibernate Error CatchBlock");
					return " " + e + " ";
				}
			} else {
				return catgeries.getCATEGORYNAME() + " Already Exist";
			}
		}
	}

	public List<Companies> ViewCompanies() {
		Session session = Connection();
		//org.hibernate.classic.Session session = Session.openSession();
		org.hibernate.Query cq = session.createQuery("from Companies");
		List<Companies> list = cq.list();
		return list;
	}

	public List<Categeries> ViewCategories() {
		Session session = Connection();
		//org.hibernate.classic.Session session = Session.openSession();
		org.hibernate.Query cq = session.createQuery("from Categeries");
		List<Categeries> list = cq.list();
		return list;
	}

	@Override
	public String addItem(Companies company, Categeries category, Items items) {
		Session connection = Connection();
		//org.hibernate.classic.Session connection = Session.openSession();
		Companies Result1 = (Companies) connection.get(Companies.class, company.getCOMPANYID());
		Categeries Result2 = (Categeries) connection.get(Categeries.class, category.getCATEGORYID());
		System.out.println(
				Result2.getCATEGORYID() + "-------------------------------------------" + Result1.getCOMPANYID());
		items.setCategeryid(Result2);
		items.setCompanyid(Result1);
		connection.save(items);
		connection.beginTransaction().commit();
		return null;
	}
}
