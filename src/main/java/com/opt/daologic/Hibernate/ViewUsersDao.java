package com.opt.daologic.Hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import com.opt.daologic.DaoInterfaces.IViewUsersDao;
import com.opt.pojos.Users;

public class ViewUsersDao implements IViewUsersDao {
	private final static Logger logger = Logger.getLogger(ViewUsersDao.class);

	private void init() {
		logger.info(ViewUsersDao.class + " Initiated");
	}

	private static Session Connection() {
		SessionFactory sessionfact = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionfact.openSession();
		logger.info("Connection Created");
		return session;
	}

	public List<Users> ViewUsers(int pageid, int total) {
		Session session = Connection();
		org.hibernate.Query cq = session.createQuery("from Users");
		cq.setFirstResult(pageid);
		cq.setMaxResults(total);
		List<Users> list = cq.list();
		return list;
	}

	public List<Users> DeleteUsers(String[] Id) {
		logger.info("Entered to DeleteUserHibernate Method");
		Session connection = Connection();
		for (String string : Id) {
			connection.beginTransaction();
			Users user = (Users) connection.get(Users.class, Integer.parseInt(string));
			if (user.getProle().equals("Administator")) {

			} else {
				connection.delete(user);
				connection.getTransaction().commit();
			}
		}
		org.hibernate.Query user = connection.createQuery("from Users");
		List<Users> res = user.list();
		logger.info("Exit From DeleteuserHibernate Method");
		return res;
	}

	public List<Users> SearchUsers(String uname) {
		logger.info("Entered To SearchUsersHibernate Method");
		Session session = Connection();
		Criteria query = session.createCriteria(Users.class);
		query.add(Restrictions.like("uname", uname + "%"));
		List<Users> result = query.list();
		logger.info("Exit From SearchUserHibernate Method");
		return result;
	}

}
