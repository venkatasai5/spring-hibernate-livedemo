package com.opt.controllers;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.opt.daologic.DaoInterfaces.IIndexDao;
import com.opt.pojos.Account;
import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;
import com.opt.pojos.Messages;
import com.opt.pojos.TextMsg;
import com.opt.pojos.Users;
import com.validation.EmailValidation;
import com.validation.PasswordValidation;

@Controller
public class Index {
	private final static Logger logger = Logger.getLogger(Index.class);
	private static String OTP = String.valueOf(OTPGenerator.OTP(6));
	private int count = 1;

	@Autowired
	IIndexDao hb;

	@RequestMapping(value = "/RegisterValidate", method = RequestMethod.POST)
	public ModelAndView Register(Users users, HttpServletRequest request, Model model) {
		logger.info("Entered to registered Save Method");
		boolean email = EmailValidation.emailvalidation(users.getEmail());
		PasswordValidation pv = new PasswordValidation();
		final Map<Integer, String> password = pv.passwordValidation(users.getUname(), users.getPwd());
		Boolean passwordFlag = false;
		String passwordFlagMsg = "";
		for (Entry<Integer, String> entry : password.entrySet()) {
			if (entry.getKey() == 0) {
				passwordFlag = Boolean.valueOf(entry.getValue());
			} else {
				passwordFlagMsg = entry.getValue();
			}
		}
		if (passwordFlag) {
			if (email) {
				try {
					Users result = hb.RegisterSave(users);
					if (result != null) {
						return new ModelAndView("Index", "Regmsg", "Registration Successfully U can Login Now!!");
					} else {
						model.addAttribute("name", users.getUname());
						model.addAttribute("phno", users.getPhno());
						model.addAttribute("email", users.getEmail());
						model.addAttribute("dob", users.getDob());
						return new ModelAndView("Index", "Regmsg", "Email Already In Use!!");
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					return new ModelAndView("Index", "Regmsg", "Registration Failed: 500 Internal Error!");
				}
			}
			model.addAttribute("name", users.getUname());
			model.addAttribute("phno", users.getPhno());
			model.addAttribute("dob", users.getDob());
			return new ModelAndView("Index", "emailmsg1", "Enter The Correct Email");
		}
		model.addAttribute("name", users.getUname());
		model.addAttribute("phno", users.getPhno());
		model.addAttribute("email", users.getEmail());
		model.addAttribute("dob", users.getDob());
		return new ModelAndView("Index", "emailmsg2", "Enter The Correct Password");
	}

	@RequestMapping(value = { "/LoginSuccess", "/sendOTP" })
	public String authorizationPage(Principal p) {
		try {
			String uname = p.getName();
			Users Profile = hb.LoginValidate(uname);

			String text = "Your Login Verification Code: " + OTP;

			final String url = "https://www.smsgatewayhub.com/api/mt/SendSMS";

			Account account = new Account();
			account.setUser("Venkatasai");
			account.setPassword("Iambadboy");
			account.setSenderid("TESTIN");
			account.setChannel(2);
			account.setDCS(0);

			Messages msg = new Messages();
			msg.setNumber(Profile.getPhno());
			msg.setText(text);

			List<Messages> mesgs = new ArrayList<>();
			mesgs.add(msg);

			TextMsg input = new TextMsg();
			input.setAccount(account);
			input.setMessages(mesgs);
			Gson json = new Gson();
			String payload = json.toJson(input);
			// set headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(payload, headers);

			RestTemplate restTemplate = new RestTemplate();

			// send request and parse result
			restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			return "UserAthorization";
		} catch (Exception e) {
		}
		return "";

	}

	@RequestMapping(value = "/OPTvalidate")
	public ModelAndView loginSuccess(Principal p, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		String uname = p.getName();
		String otp = request.getParameter("otp");
		if (otp.equals(OTP)) {
			Users Profile = hb.LoginValidate(uname);
			if (Profile.getProle().equals("Administator")) {
				session.setAttribute("Amsg", Profile);
				return new ModelAndView("Admin");
			} else if (Profile.getProle().equals("User")) {
				List<Companies> Companies = hb.CompanyList();
				List<Categeries> Categeries = hb.CategeriesList();
				List<Items> Items = hb.ItemList();
				session.setAttribute("Companies", Companies);
				session.setAttribute("Categeries", Categeries);
				session.setAttribute("Items", Items);
				session.setAttribute("Amsg", Profile);
				return new ModelAndView("User");
			} else {
				return new ModelAndView("Index", "Loginmsg", "Something Went Wrong");
			}
		} else if (count == 3) {
			count = 0;
			OTP = null;
			return new ModelAndView("Index");
		} else {
			count += 1;

			return new ModelAndView("UserAthorization", "otpmsg", "You Have Last " + (4 - count) + " Attepmts!!!");
		}
	}

	@RequestMapping(value = "/LoginFailure")
	public ModelAndView loginFailure(Principal p, HttpServletRequest req) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		String uname = (String) session.getAttribute("uname");
		String username1 = (String) req.getSession(true).getAttribute("SPRING_SECURITY_LAST_USERNAME");
		String username2 = (String) req.getSession(true).getAttribute("SPRING_SECURITY_LAST_US‌​ERNAME_KEY");
		System.out.println(username1);
		System.out.println(username2);
		System.out.println(uname);
		Users Profile = hb.LoginValidate(p.getName());
		if (Profile.equals(null)) {
			return new ModelAndView("Index", "Loginmsg", "User Doesn't Exist Plz Register");
		} else {
			return new ModelAndView("Index", "pwd", "Enter The Correct Password");
		}
	}

	@RequestMapping(value = "/Logout")
	public String Logout(HttpServletRequest req) {
		hb.Logout();
		req.getSession().invalidate();
		return "Index";
	}

	@RequestMapping(value = { "/", "/403" })
	public String Boot(Principal p) {
		ModelAndView model = new ModelAndView();
		model.addObject("deneid", "Access Denied!!");
		return "Index";
	}
}

class OTPGenerator {
	static char[] OTP(int len) {
		// Using numeric values
		String numbers = "0123456789";

		// Using random method
		Random rndm_method = new Random();

		char[] otp = new char[len];

		for (int i = 0; i < len; i++) {
			otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return otp;
	}
}
