package com.opt.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.opt.daologic.DaoInterfaces.IAddProductsDao;
import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;

@Controller
public class AddProducts {
	private final static Logger logger = Logger.getLogger(AddProducts.class);

	@Autowired
	IAddProductsDao hb;

	@ResponseBody
	@RequestMapping(value = "/AddProducts")
	public String AddProduct(Model model) {
		List<Companies> list1 = hb.ViewCompanies();
		List<Categeries> list2 = hb.ViewCategories();
		model.addAttribute("CompanyList", list1);
		model.addAttribute("CategoryList", list2);		
		return "AddProducts";
	}

	@RequestMapping(value = "/AddCompanyAndCategory")
	public ModelAndView add(Companies companies, Categeries catgeries) {
		logger.info("Entered Into AddCompany And Category Method");
		String Result1 = hb.addCampany(companies);
		logger.info("Exit from AddCompany Method");
		String Result2 = hb.addCategory(catgeries);
		logger.info("Exit From AddCategory Method");
		if (Result1 == null && Result2 == null) {
			return new ModelAndView("AddProducts", "AddCCError", "Blanks Are Empty");
		} else if (Result1 == null) {
			return new ModelAndView("AddProducts", "AddCCError", Result2);
		} else if (Result2 == null) {
			return new ModelAndView("AddProducts", "AddCCError", Result1);
		} else {
			return new ModelAndView("AddProducts", "AddCCError", Result1 + " && " + Result2);
		}
	}

	@RequestMapping(value = "/itemadd")
	public String add(Companies company, Categeries category, Items items, Model model) {
		logger.info("Entered Into AddItems Controller Mathod");
		hb.addItem(company, category, items);
		return "AddProducts";
	}
}
