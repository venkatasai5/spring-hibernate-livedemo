package com.opt.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.opt.daologic.DaoInterfaces.IViewProductsDao;
import com.opt.pojos.Items;

@Controller
public class ViewProducts {
	private final static Logger logger = Logger.getLogger(ViewProducts.class);

	@Autowired
	IViewProductsDao hb;

	@RequestMapping(value = "/ViewProducts")
	public String ViewProduct(Model model) {
		logger.info("Entered Into ViewProductsController Methods");
		List<Items> result = hb.ViewProducts();
		model.addAttribute("itmes", result);
		return "ViewProducts";
	}
}
