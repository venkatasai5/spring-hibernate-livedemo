package com.opt.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.opt.daologic.DaoInterfaces.IUserViewDao;
import com.opt.pojos.Categeries;
import com.opt.pojos.Companies;
import com.opt.pojos.Items;

public class UserView {
	private final static Logger logger = Logger.getLogger(Index.class);

	@Autowired
	IUserViewDao dao;

	@RequestMapping(value = "/Home")
	public ModelAndView Home(HttpServletRequest request) {
		logger.info("Entered to User Controller Method");
		HttpSession session = request.getSession(true);
		List<Companies> Companies = dao.CompanyList();
		List<Categeries> Categeries = dao.CategeriesList();
		List<Items> Items = dao.ItemList();
		session.setAttribute("Companies", Companies);
		session.setAttribute("Categeries", Categeries);
		session.setAttribute("Items", Items);
		return new ModelAndView("User");

	}

}
