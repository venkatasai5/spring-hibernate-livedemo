package com.opt.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.opt.daologic.DaoInterfaces.IViewUsersDao;
import com.opt.pojos.Users;

@Controller
public class ViewUsers {
	private final static Logger logger = Logger.getLogger(ViewUsers.class);

	@Autowired
	IViewUsersDao hb;

	@RequestMapping(value = "/ViewUsers/{pageid}")
	public ModelAndView ViewUser(@PathVariable int pageid) {
		int total = 5;
		if (pageid == 1) {
		} else {
			pageid = pageid - 1 * total + 1;
		}
		List<Users> list = hb.ViewUsers(pageid, total);
		
		return new ModelAndView("ViewUsers", "ViewUsers", list);
	}

	@RequestMapping(value = "/deleteUsers")
	public String DeleteUsers(@RequestParam(value = "checkId") String[] Id, Model model) {
		logger.info("entered to Delete Method");
		List<Users> result = hb.DeleteUsers(Id);
		model.addAttribute("ViewUsers", result);
		model.addAttribute("DeleteUsers", "Deleted Successfully");
		return "ViewUsers";
	}

	@RequestMapping(value = "/searchUsers")
	public ModelAndView search(@RequestParam(value = "search") String uname) {
		List<Users> result = hb.SearchUsers(uname);
		if (result.isEmpty()) {
			return new ModelAndView("ViewUsers", "SearchUsers", "Empty Records");
		} else {
			return new ModelAndView("ViewUsers", "ViewUsers", result);
		}
	}

}
