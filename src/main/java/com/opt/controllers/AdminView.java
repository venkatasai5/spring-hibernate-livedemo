package com.opt.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.opt.daologic.DaoInterfaces.IAdminViewDao;
import com.opt.daologic.Hibernate.HomeDao;
import com.opt.pojos.Users;

@Controller
public class AdminView {
	private final static Logger logger = Logger.getLogger(HomeDao.class);

	@Autowired
	IAdminViewDao hb;

	@RequestMapping(value = "/Admin")
	public String Admin(Model model) {
		return "Admin";
	}

	@RequestMapping(value = "/UpdateProfile", method = RequestMethod.POST)
	public String UpdateProfile(HttpServletRequest req, Principal p) {
		Users result = hb.ProfileUpdateAction(p.getName());
		HttpSession session = req.getSession(false);
		session.setAttribute("Updatemsg", result);
		logger.info("Exit ProfileUpdateAction Method");
		return "ProfileUpdate";
	}

	@RequestMapping(value = "/updateprofileProcess", method = RequestMethod.POST)
	public String Updateprocess(Users users, Model model, HttpServletRequest req) {
		HttpSession session = req.getSession();
		try {
			Users result = hb.ProfileUpdateProcess(users);
			session.setAttribute("Amsg", result);
			model.addAttribute("Amsg1", "Update Success");
			return "Admin";
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("error", "Not able to Update");
			return "ProfileUpdate";
		}
	}
}
