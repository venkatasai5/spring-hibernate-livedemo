package com.opt.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "Register")
public class Users {

	@Column(name = "uname")
	private String uname;

	@Column(name = "prole")
	private String prole;

	@Column(name = "pwd")
	private String pwd;

	@Column(name = "dob")
	private String dob;
	@Id
	@Column(name = "email")
	private String email;

	@Column(name = "phno")
	private String phno;

	@Column(name = "date_time")
	private String date_time;

	@Column(name = "status")
	private Integer status;

	@Column(name = "count")
	private Integer count;

	//@Version
	//@Column(name = "version")
	//private long version;

	public final String getUname() {
		return uname;
	}

	public final void setUname(String uname) {
		this.uname = uname;
	}

	public final String getProle() {
		return prole;
	}

	public final void setProle(String prole) {
		this.prole = prole;
	}

	public final String getPwd() {
		return pwd;
	}

	public final void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public final String getDob() {
		return dob;
	}

	public final void setDob(String dob) {
		this.dob = dob;
	}

	public final String getEmail() {
		return email;
	}

	public final void setEmail(String email) {
		this.email = email;
	}

	public final String getPhno() {
		return phno;
	}

	public final void setPhno(String phno) {
		this.phno = phno;
	}

	public final String getDate_time() {
		return date_time;
	}

	public final void setDate_time(String date_time) {
		this.date_time = date_time;
	}

	public final Integer getStatus() {
		return status;
	}

	public final void setStatus(Integer status) {
		this.status = status;
	}

	public final Integer getCount() {
		return count;
	}

	public final void setCount(Integer count) {
		this.count = count;
	}

	//public final long getVersion() {
	//	return version;
	//}

	//public final void setVersion(long version) {
	//	this.version = version;
	//}
}
