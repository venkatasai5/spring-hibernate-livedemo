package com.opt.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.stereotype.Repository;

@Entity
@Repository
@Table(name = "CATEGORIESNAME")
public class Categeries {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "COTEGORIES_SEQ")
	@SequenceGenerator(name = "COTEGORIES_SEQ", sequenceName = "COTEGORIES_SEQ")
	private Integer CATEGORYID;
	private String CATEGORYNAME;

/*	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "COMPANYCATEGORY_Items", joinColumns = {
			@JoinColumn(name = "CATEGORYID") }, inverseJoinColumns = { @JoinColumn(name = "Itemid"), })
	private List<Items> items;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "COMPANYCATEGORY_Items", joinColumns = {
			@JoinColumn(name = "CATEGORYID") }, inverseJoinColumns = { @JoinColumn(name = "COMPANYID") })
	private List<Companies> companies;
*/
	public Integer getCATEGORYID() {
		return CATEGORYID;
	}

	public void setCATEGORYID(Integer cATEGORYID) {
		CATEGORYID = cATEGORYID;
	}

	public String getCATEGORYNAME() {
		return CATEGORYNAME;
	}

	public void setCATEGORYNAME(String cATEGORYNAME) {
		CATEGORYNAME = cATEGORYNAME;
	}

/*	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}

	public List<Companies> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Companies> companies) {
		this.companies = companies;
	}
*/}
