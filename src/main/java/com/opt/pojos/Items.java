package com.opt.pojos;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ItemsTable")
public class Items {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "item_seq")
	@SequenceGenerator(name = "item_seq", sequenceName = "item_seq")
	@Column(name = "itemid")
	private Integer itemId;
	@Column(name = "iname")
	private String item;
	@Column(name = "quantity")
	private Integer quantity;
	@Column(name = "price")
	private Integer price;
	@Column(name = "features")
	private String featuers;
	@Column(name = "links")
	private String links;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "CATEGORIES_Items", joinColumns = { @JoinColumn(name = "Itemid") }, inverseJoinColumns = {
			@JoinColumn(name = "CATEGORYID") })
	private Categeries categeryid;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "COMPANY_Items", joinColumns = { @JoinColumn(name = "Itemid") }, inverseJoinColumns = {
			@JoinColumn(name = "COMPANYID") })
	private Companies companyid;

	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getFeatuers() {
		return featuers;
	}

	public void setFeatuers(String featuers) {
		this.featuers = featuers;
	}

	public String getLinks() {
		return links;
	}

	public void setLinks(String links) {
		this.links = links;
	}

	public Categeries getCategeryid() {
		return categeryid;
	}

	public void setCategeryid(Categeries categeryid) {
		this.categeryid = categeryid;
	}

	public Companies getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Companies companyid) {
		this.companyid = companyid;
	}
}
