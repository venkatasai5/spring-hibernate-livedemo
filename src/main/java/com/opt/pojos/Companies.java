package com.opt.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.stereotype.Repository;

@Entity
@Repository
@Table(name = "COMPANIESNAME")
public class Companies {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "COMPANIESNAME_SEQ")
	@SequenceGenerator(name = "COMPANIESNAME_SEQ", sequenceName = "COMPANIESNAME_SEQ")
	private Integer COMPANYID;
	private String COMPANYNAME;

	/*
	 * @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	 * 
	 * @JoinTable(name = "COMPANYCATEGORY_Items", joinColumns = { @JoinColumn(name =
	 * "COMPANYID") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "CATEGORYID") }) private List<Categeries> CATEGORIES;
	 * 
	 * @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	 * 
	 * @JoinTable(name = "COMPANYCATEGORY_Items", joinColumns = { @JoinColumn(name =
	 * "COMPANYID") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "Itemid") }) private List<Items> items;
	 */
	public Integer getCOMPANYID() {
		return COMPANYID;
	}

	public void setCOMPANYID(Integer cOMPANYID) {
		COMPANYID = cOMPANYID;
	}

	public String getCOMPANYNAME() {
		return COMPANYNAME;
	}

	public void setCOMPANYNAME(String cOMPANYNAME) {
		COMPANYNAME = cOMPANYNAME;
	}

	/*
	 * public List<Categeries> getCATEGORIES() { return CATEGORIES; }
	 * 
	 * public void setCATEGORIES(List<Categeries> cATEGORIES) { CATEGORIES =
	 * cATEGORIES; }
	 * 
	 * public List<Items> getItems() { return items; }
	 * 
	 * public void setItems(List<Items> items) { this.items = items; }
	 */}
